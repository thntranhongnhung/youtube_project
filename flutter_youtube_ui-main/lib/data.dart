class User {
  final String username;
  final String profileImageUrl;
  final String subscribers;

  const User({
    required this.username,
    required this.profileImageUrl,
    required this.subscribers,
  });
}

const User currentUser = User(
  username: 'Eienno Rosa',
  profileImageUrl:
      'https://library.sportingnews.com/styles/twitter_card_120x120/s3/2021-08/lionel-messi-fc-barcelona-august-8-2021_k68gw0xpdt6c1gb9tt3xccuhp.jpg?itok=N09zPHde',
  subscribers: '2804k',
);
const User userOne = User(
  username: 'Billie Eilish',
  profileImageUrl:
  'https://akamai.vgc.no/v2/images/e663c104-5fc5-4b13-9375-98236786ff16?fit=crop&format=auto&h=1779&w=1900&s=8dd033c0eae00744c0869a263a89662fb987df3e',
  subscribers: '666k',
);
const User userTwo = User(
  username: 'Fad3nHD',
  profileImageUrl:
  'https://file.qdnd.vn/data/images/0/2021/06/29/huutruong/lionel-messi.jpg?dpi=150&quality=100&w=575',
  subscribers: '666k',
);
const User userThree = User(
  username:
      'Elon Musk Rewind',
  profileImageUrl:
  'https://c.ndtvimg.com/2022-04/in3v9jbg_elon-musk-650_650x400_04_April_22.jpg',
  subscribers: '666k',
);
const User userFour = User(
  username:
   'Zedd',
  profileImageUrl:
  'https://i.ytimg.com/vi/n1a7o44WxNo/maxresdefault.jpg',
  subscribers: '6M',
);
const User userFive = User(
  username:
  'Elon Musk Rewind',
  profileImageUrl:
  'https://c.ndtvimg.com/2022-04/in3v9jbg_elon-musk-650_650x400_04_April_22.jpg',
  subscribers: '666k',
);
const User userSix = User(
  username:
  'Phê Phim',
  profileImageUrl:
  'https://yt3.ggpht.com/ytc/AKedOLTh6ReF7VfrX_xU4eJpod-k_zG6r8xOnCRIXD3BZQ=s900-c-k-c0x00ffffff-no-rj',
  subscribers: '1M5',
);
class Video {
  final String id;
  final User author;
  final String title;
  final String thumbnailUrl;
  final String duration;
  final DateTime timestamp;
  final String viewCount;
  final String likes;
  final String dislikes;

  const Video({
    required this.id,
    required this.author,
    required this.title,
    required this.thumbnailUrl,
    required this.duration,
    required this.timestamp,
    required this.viewCount,
    required this.likes,
    required this.dislikes,
  });
}

final List<Video> videos = [
  Video(
    id: 'x606y4QWrxo',
    author: userOne,
    title: 'Billie Eilish - Happier Than Ever (Official Lyric Video)',
    thumbnailUrl: 'https://fm100.com/wp-content/uploads/sites/12/2021/05/hea-billieilish1200x630.jpg',
    duration: '3:20',
    timestamp: DateTime(2021, 7, 30),
    viewCount: '34M',
    likes: '660k',
    dislikes: '500',
  ),
  Video(
    author: userThree,
    id: 'vrPk6LB9bjo',
    title:
        'Elon Musk Talks About Aliens',
    thumbnailUrl: 'https://ichef.bbci.co.uk/news/976/cpsprodpb/9D42/production/_124485204_tv075719297.jpg',
    duration: '8:06',
    timestamp: DateTime(2021, 12, 16),
    viewCount: '600K',
    likes: '12K',
    dislikes: '9',
  ),
  Video(
    id: 'ilX5hnH8XoI',
    author: userTwo,
    title: 'Lionel Messi - Top 20 Goals of The GOAT - HD',
    thumbnailUrl: 'https://vnn-imgs-f.vgcloud.vn/2021/07/11/12/messi-copa-america-2021-2.jpg',
    duration: '10:53',
    timestamp: DateTime(2020, 3, 31),
    viewCount: '5M',
    likes: '67k',
    dislikes: '400',
  ),
];

final List<Video> suggestedVideos = [
  Video(
    id: 'rJKN_880b-M',
    author: userFour,
    title: 'Zedd - Beautiful Now ft. Jon Bellion (Official Music Video)',
    thumbnailUrl: 'https://i.ytimg.com/vi/Sl2HeP8RlfU/maxresdefault.jpg',
    duration: '13:15',
    timestamp: DateTime(2015, 6, 12),
    viewCount: '32M',
    likes: '1.9k',
    dislikes: '7',
  ),
  Video(
    id: 'HvLb5gdUfDE',
    author: userFive,
    title: 'Thám Tử Lừng Danh Conan - Tập 460 - Tiếng "A" cuối cùng - Trọn Bộ Conan',
    thumbnailUrl: 'https://pic-bstarstatic.akamaized.net/ugc/f6b2e1470c039be9f3d9083102862920caa86838.jpg@1200w_630h_1e_1c_1f',
    duration: '1:52:12',
    timestamp: DateTime(2022, 2, 23),
    viewCount: '1M9K',
    likes: '9.3K',
    dislikes: '45',
  ),
  Video(
    id: 'h-igXZCCrrc',
    author: userSix,
    title: 'VÌ SAO MONEY HEIST LÀ VỤ CƯỚP VĨ ĐẠI NHẤT MÀN ẢNH NHỎ?',
    thumbnailUrl: 'https://cdn1.tuoitre.vn/zoom/600_315/2021/12/7/money-heist-0-1638860738389953190162-crop-16388607605791468365275.jpeg',
    duration: '1:52:58',
    timestamp: DateTime(2020, 04, 18),
    viewCount: '1M',
    likes: '500k',
    dislikes: '2N',
  ),
];
